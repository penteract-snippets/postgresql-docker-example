# Postgres Local Setup With Docker And Node.

This project provides examples of setting up and interacting with Postgresql
on your local machine with docker.

## Project Dependencies
- node
- docker

> Note: Ensure that you can run `docker ps` without the use of sudo/please for more information [follow this link](https://docs.docker.com/install/linux/linux-postinstall/)

## Project installation

```bash
  npm install
```

## Running the Project

Start Postgres:

```bash
npm run startPostgres
```

Connect to Postgres:

```bash
npm run connectToPostgres
```

Run Node app

```bash
npm start
```

